<?php

namespace App;

class Course_name{

    private $physics;
    private $markphysics;

    private $chemistry;
    private $markchemistry;

    private $highermath;
    private $markhighermath;

    private $biology;
    private $markbiology;

    public function setphysics($physics)
    {
        $this->physics = $physics;
    }

    public function setmarkphysics()
    {
        $this->markphysics = $this->Gradeinfo($this->physics);
    }

    public function setchemistry($chemistry)
    {
        $this->chemistry = $chemistry;
    }

    public function setmarkchemistry()
    {
        $this->markchemistry = $this->Gradeinfo($this->chemistry);
    }

    public function sethighermath($highermath)
    {
        $this->highermath = $highermath;
    }

    public function setmarkhighermath()
    {
        $this->markhighermath = $this->Gradeinfo($this->highermath) ;
    }

    public function setBiology($biology)
    {
        $this->biology = $biology;
    }

    public function setMarkbiology()
    {
        $this->markbiology = $this->Gradeinfo($this->biology);
    }

    public function getphysics()
    {
        return $this->physics;
    }

    public function getmarkphysics()
    {
        return $this->markphysics;
    }

    public function getchemistry()
    {
        return $this->chemistry;
    }

    public function getmarkchemistry()
    {
        return $this->markchemistry;
    }

    public function gethighermath()
    {
        return $this->highermath;
    }

    public function getmarkhighermath()
    {
        return $this->markhighermath;
    }

    public function getBiology()
    {
        return $this->biology;
    }

    public function getMarkbiology()
    {
        return $this->markbiology;
    }

    public function Gradeinfo($mark){

        switch($mark){

            case ($mark>79) :
                $grade = "A+";
                break;

            case ($mark>69) :
                $grade = "A";
                break;

            case ($mark>59) :
                $grade = "A-";
                break;

            case ($mark>49) :
                $grade = "B";
                break;

            case ($mark>39) :
                $grade = "C";
                break;

            case ($mark<33) :
                $grade = "Failed!";
                break;

        }

        return $grade;
    }


}